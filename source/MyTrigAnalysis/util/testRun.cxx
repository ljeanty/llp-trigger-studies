#include "xAODRootAccess/Init.h"
#include "SampleHandler/SampleHandler.h"
#include "SampleHandler/ScanDir.h"
#include "SampleHandler/ToolsDiscovery.h"
#include "SampleHandler/ToolsSplit.h"
#include "EventLoop/Job.h"
#include "EventLoop/DirectDriver.h"
#include "EventLoop/SlurmDriver.h" 
#include "EventLoop/SoGEDriver.h"
#include "SampleHandler/DiskListLocal.h"
#include <TSystem.h>

#include <EventLoopAlgs/NTupleSvc.h>
#include <EventLoop/OutputStream.h>

#include "MyTrigAnalysis/MyTrigAnalysis.h"

const int subLocal = 0;
const int subSLURM = 3;

int main( int argc, char* argv[] ) {

  Long64_t nEvtBatch = -1;

  // Take the submit directory from the input if provided:
  std::string submitDir = "submitDir";
  if( argc > 1 ) submitDir = argv[ 1 ];
  //std::string dsName = "mc15_13TeV.402146.PythiaRhad_AUET2BCTEQ6L1_gen_gluino_p1_2000_qq_100_30ns.recon.AOD.e4809_s2810_s2183_r7611_r7578_r8050";
  ////std::string dsName = "AOD.12330472._000541.pool.root.1";
  std::string dsName = "data17_13TeV.00327265.physics_EnhancedBias.merge.AOD.r9949_r10021_r10018_p3313";
  if( argc > 2 ) dsName = argv[ 2 ];
  int nEvents = 10000;
  if( argc > 3 ) nEvents = std::atoi(argv[ 3 ]);
  if( argc > 4 ) nEvtBatch = std::atoi(argv[ 4 ]); //submit to batch nevents/job

  //set the submission type
  int submissionType = subSLURM;

  // Set up the job for xAOD access:
  xAOD::Init().ignore();

  // Construct the samples to run on:
  SH::SampleHandler sh;

  //const char * basePath = "/global/projecta/projectdirs/atlas/spgriso/data/LLPWorkshop17/FTKSim/AOD/";
  ////const char * basePath = "/global/projecta/projectdirs/atlas/spgriso/code/LLPTriggerFTK/data/data17_13TeV.00327265.physics_EnhancedBias.merge.AOD.r9949_r10021_r10018_p3313";
  const char * basePath = "/global/projecta/projectdirs/atlas/spgriso/data/AtlasFtkTrkIso/";
  // use SampleHandler to scan all of the subdirectories of a directory for particular MC single file:
  const char* inputFilePath = gSystem->ExpandPathName (basePath);
  SH::ScanDir()
      .sampleDepth(0)
      .samplePattern(dsName)
      .filePattern("AOD.*.pool.root*")
      .scan(sh,inputFilePath);

  // Set the name of the input TTree. It's always "CollectionTree"
  // for xAOD files.
  sh.setMetaString( "nc_tree", "CollectionTree" );

  // Print what we found:
  sh.print();

  // Create an EventLoop job:
  EL::Job job;
  job.sampleHandler( sh );
  job.options()->setDouble (EL::Job::optMaxEvents, nEvents);
  job.options()->setString (EL::Job::optXaodAccessMode,
                            EL::Job::optXaodAccessMode_athena);

  // Adding output tuple:
  EL::OutputStream output  ("ftkAnaOutput");
  job.outputAdd (output);
  EL::NTupleSvc *ntuple = new EL::NTupleSvc ("ftkAnaOutput");
  job.algsAdd (ntuple);

  // Add our analysis to the job:
  MyTrigAnalysis* alg = new MyTrigAnalysis();
  alg->outputName = "ftkAnaOutput";
  job.algsAdd( alg );

  // Run the job using the appropriate driver:
  if (submissionType == subLocal) {
    //local submission
    if (nEvtBatch > 0) {
      EL::SoGEDriver driver;
      sh.setMetaDouble("optEventsPerWorker", nEvtBatch);
      driver.shellInit = "shopt -s expand_aliases\n";
      driver.options()->setString("optSubmitFlags", "-S /bin/bash -l cvmfs=1,h_vmem=3G");
      driver.submitOnly( job, submitDir );
    } else {
      EL::DirectDriver driver;
      driver.submit( job, submitDir );
    }
  }
  else if (submissionType == subSLURM) {
    //batch submission
     EL::SlurmDriver driver;
    driver.SetJobName(submitDir);
    driver.SetAccount("atlas");
    driver.SetPartition("shared-chos"); //for chos
    //driver.SetPartition("shared"); //for shifter
    driver.SetRunTime("3:00:00");
    //NOTE this is supposed to be 4000 now
    driver.SetMemory("4000"); //units: MB. Note: if you exceed 1.9G you will be using multiple cores!
    driver.SetConstrain(""); //Example could be access to /project or /projecta
    driver.options()->setString(EL::Job::optBatchSlurmExtraConfigLines, "#SBATCH  --image=custom:pdsf-chos-sl64:v4\n export TMPDIR=${SLURM_TMP}\n export PATH=./:${PATH}");
    driver.options()->setString(EL::Job::optBatchSlurmWrapperExec, "CHOS=sl64 chos "); //this is for running with chos
    //driver.options()->setString(EL::Job::optBatchSlurmWrapperExec, "CHOS=sl64 chos $xx"); //this is for running with chos
    //driver.options()->setString(EL::Job::optBatchSlurmWrapperExec, "shifter  --volume=/global/project:/project  --volume=/global/projecta:/projecta  /bin/bash $xx"); //this is for running with shifter (will be the default soon)
    //driver.options()->setString(EL::Job::optBatchSlurmWrapperExec, "shifter  --volume=/global/project:/project  --volume=/global/projecta:/projecta  /bin/bash ");
    SH::scanNEvents(sh);
    sh.setMetaDouble(EL::Job::optEventsPerWorker, 10000);
    //driver.options()->setString(EL::Job::optSubmitFlags, "-S /bin/bash -l cvmfs=1,h_vmem=3G"); //@Jan: set here SLURM specific options! See also the example above for SGE case
    driver.submitOnly(job, submitDir );
  }


  return 0;
}
