#include <EventLoop/Job.h>
#include <EventLoop/StatusCode.h>
#include <EventLoop/Worker.h>
#include <MyTrigAnalysis/MyTrigAnalysis.h>

// Infrastructure include(s):
#include "xAODRootAccess/Init.h"
#include "xAODRootAccess/TEvent.h"
#include <TSystem.h>
#include <TFile.h>

// Histograms and functions
#include <TH1.h>

// ASG status code check
#include <AsgTools/MessageCheck.h>

// EDM includes:
#include "xAODEventInfo/EventInfo.h"
#include "xAODMuon/MuonContainer.h"

#include "xAODTrigger/MuonRoIContainer.h"
#include "xAODTrigger/JetRoIContainer.h"
#include "xAODTrigger/EmTauRoIContainer.h"
#include <xAODTruth/TruthParticleContainer.h>
#include <xAODTruth/TruthVertexContainer.h>

#include "xAODTrigMissingET/TrigMissingETContainer.h"


#include "xAODTracking/TrackParticleContainer.h"
#include "xAODTracking/VertexContainer.h"

// this is needed to distribute the algorithm to the workers
ClassImp(MyTrigAnalysis)

bool isRHadron(const xAOD::TruthParticle& part) {
  if (part.status() != 1) return false;
  static const std::vector<int> pdgids = {1000993, 1009113, 1009213, 1009223, 1009313, 1009323, 1009333, 1091114, 1092114, 1092214, 1092224, 1093114, 1093214, 1093224, 1093314, 1093324, 1093334, 1000612, 1000622, 1000632, 1000642, 1000652, 1006113, 1006211, 1006213, 1006223, 1006311, 1006313, 1006321, 1006323, 1006333};
  if (std::find(pdgids.begin(), pdgids.end(), part.absPdgId()) == pdgids.end()) 
    return false;
  return true;
}

bool isChargedRHadron(const xAOD::TruthParticle& part) {
  if (part.status() != 1) return false;
  static const std::vector<int> pdgids = {1009213, 1009323, 1091114, 1092214, 1092224, 1093114, 1093224, 1093314, 1093334, 1000612, 1000632, 1000652, 1006211, 1006213, 1006223, 1006321, 1006323};
  if (part.barcode() >= 200000) return false;
  if (std::find(pdgids.begin(), pdgids.end(), part.absPdgId()) == pdgids.end()) 
    return false;
  return true;
}

bool isRHadronDaughter(const xAOD::TruthParticle& part) {
  if (part.status() != 1) return false;
  const xAOD::TruthParticle* parent = part.parent();
  if (!parent) return false;
  return (isRHadron(*parent));
}

const xAOD::TruthParticle* getTruthParticle(const xAOD::IParticle* p){
  typedef ElementLink< xAOD::TruthParticleContainer > Link_t;
  static const char* NAME = "truthParticleLink";
  if( ! p->isAvailable< Link_t >( NAME ) ) {
    return 0;
  }
  const Link_t& link = p->auxdata< Link_t >( NAME );
  if( ! link.isValid() ) {
    return 0;
  }
  return *link;
}

float deltaPhi(float phi1, float phi2) {
  float dphi = fabs(phi1-phi2);
  if (dphi > TMath::Pi())
    dphi = 2.*TMath::Pi() - dphi;
  return dphi;
}
float getDeltaR(const xAOD::IParticle* p1, const xAOD::IParticle* p2){
  float delta_eta = fabs( p2->eta() - p1->eta() );
  float delta_phi = deltaPhi(p2->phi(), p1->phi());
  float delta_R = sqrt( delta_phi*delta_phi + delta_eta*delta_eta );
  return delta_R;
}

float getDecayRadius(const xAOD::TruthParticle& part) {
  if (part.hasDecayVtx()) {
    return (sqrt(part.decayVtx()->x()*part.decayVtx()->x()+part.decayVtx()->y()*part.decayVtx()->y()+part.decayVtx()->z()*part.decayVtx()->z()));
  }
  else return -1;
}

MyTrigAnalysis :: MyTrigAnalysis ()
{
  // Here you put any code for the base initialization of variables,
  // e.g. initialize all pointers to 0.  Note that you should only put
  // the most basic initialization here, since this method will be
  // called on both the submission and the worker node.  Most of your
  // initialization code will go into histInitialize() and
  // initialize().
}



EL::StatusCode MyTrigAnalysis :: setupJob (EL::Job& job)
{
  // Here you put code that sets up the job on the submission object
  // so that it is ready to work with your algorithm, e.g. you can
  // request the D3PDReader service or add output files.  Any code you
  // put here could instead also go into the submission script.  The
  // sole advantage of putting it here is that it gets automatically
  // activated/deactivated when you add/remove the algorithm from your
  // job, which may or may not be of value to you.

  job.useXAOD ();
  ANA_CHECK_SET_TYPE (EL::StatusCode); // set type of return code you are expecting (add to top of each function once)
  ANA_CHECK(xAOD::Init());
  
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode MyTrigAnalysis :: histInitialize ()
{
  // Here you do everything that needs to be done at the very
  // beginning on each worker node, e.g. create histograms and output
  // trees.  This method gets called before any input files are
  // connected.

  TFile *outputFile = wk()->getOutputFile (outputName);
  if(!outputFile) {
    ATH_MSG_ERROR("Cannot get output file" << outputName);
    return EL::StatusCode::FAILURE;
  }
  m_outTree = m_ftkAnaOut.getTree ("tree", "tree");
  m_outTree->SetDirectory (outputFile);

  z0pvz_hist = new TH1F("z0pvz_hist","z0-primary vertex z",100,-100,100);
  ibl_hits_hist = new TH1I("ibl_hits_hist","ibl hits per track",4,0,4);
  tbl_hits_hist = new TH1I("tbl_hits_hist","second to innermost layer hits per track",4,0,4);
  wk()->addOutput(z0pvz_hist);
  wk()->addOutput(ibl_hits_hist);
  wk()->addOutput(tbl_hits_hist);

  return EL::StatusCode::SUCCESS;
}



EL::StatusCode MyTrigAnalysis :: fileExecute ()
{
  // Here you do everything that needs to be done exactly once for every
  // single file, e.g. collect a list of all lumi-blocks processed
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode MyTrigAnalysis :: changeInput (bool firstFile)
{
  // Here you do everything you need to do when we change input files,
  // e.g. resetting branch addresses on trees.  If you are using
  // D3PDReader or a similar service this method is not needed.
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode MyTrigAnalysis :: initialize ()
{
  // Here you do everything that you need to do after the first input
  // file has been connected and before the first event is processed,
  // e.g. create additional histograms based on which variables are
  // available in the input files.  You can also create all of your
  // histograms and trees in here, but be aware that this method
  // doesn't get called if no events are processed.  So any objects
  // you create here won't be available in the output if you have no
  // input events.


  ANA_CHECK_SET_TYPE (EL::StatusCode); // set type of return code you are expecting (add to top of each function once)
  
  xAOD::TEvent* event = wk()->xaodEvent(); // you should have already added this as described before

  // as a check, let's see the number of events in our xAOD
  Info("initialize()", "Number of events = %lli", event->getEntries() ); // print long long int

  // count number of events
  m_eventCounter = 0;

  // Initialize and configure trigger tools
  m_trigConfigTool = new TrigConf::xAODConfigTool("xAODConfigTool"); // gives us access to the meta-data
  ANA_CHECK( m_trigConfigTool->initialize() );
  ToolHandle< TrigConf::ITrigConfigTool > trigConfigHandle( m_trigConfigTool );
  m_trigDecisionTool = new Trig::TrigDecisionTool("TrigDecisionTool");
  ANA_CHECK(m_trigDecisionTool->setProperty( "ConfigTool", trigConfigHandle ) ); // connect the TrigDecisionTool to the ConfigTool
  ANA_CHECK(m_trigDecisionTool->setProperty( "TrigDecisionKey", "xTrigDecision" ) );
  ANA_CHECK(m_trigDecisionTool->initialize() );

  numiblhits = 0;

  //for getting unassociated vertices

  m_trktovtxtool = new CP::BaseTrackVertexAssociationTool( "BaseTrackVertexAssociationTool" );

   ANA_CHECK( m_trktovtxtool->setProperty( "OutputLevel",
                                                   MSG::ERROR ) );
   ANA_CHECK( m_trktovtxtool->setProperty( "dzSinTheta_cut", 0.5 ) );
   ANA_CHECK( m_trktovtxtool->setProperty( "d0sig_cut", 5 ) );
   ANA_CHECK( m_trktovtxtool->initialize() );

   return EL::StatusCode::SUCCESS;
}



EL::StatusCode MyTrigAnalysis :: execute ()
{
  // Here you do everything that needs to be done on every single
  // events, e.g. read input variables, apply cuts, and fill
  // histograms and trees.  This is where most of your actual analysis
  // code will go.

  ANA_CHECK_SET_TYPE (EL::StatusCode); // set type of return code you are expecting (add to top of each function once)
  xAOD::TEvent* event = wk()->xaodEvent();
  // print every 100 events, so we know where we are:
  if( (m_eventCounter % 100) ==0 ) Info("execute()", "Event number = %i", m_eventCounter );
  m_eventCounter++;
  //----------------------------
  // Event information
  //--------------------------- 
  const xAOD::EventInfo* eventInfo = 0;
  ANA_CHECK(event->retrieve( eventInfo, "EventInfo"));  
  // check if the event is data or MC
  // (many tools are applied either to data or MC)
  bool isMC = false;
  // check if the event is MC
  if(eventInfo->eventType( xAOD::EventInfo::IS_SIMULATION ) ){
       isMC = true; // can do something with this later
  }   
  m_ftkAnaOut.runNumber = eventInfo->runNumber();
  m_ftkAnaOut.eventNumber = eventInfo->eventNumber();
  m_ftkAnaOut.mu = eventInfo->averageInteractionsPerCrossing();

  //-----------------------------
  // Trigger List
  //-----------------------------
  // Print full list of available triggers on first event only
  if (m_eventCounter == -1) {
    std::vector<std::string> triggers = m_trigDecisionTool->getListOfTriggers();
    
    std::cout<<"*****************************"<<std::endl;
    std::cout<<" LIST OF TRIGGERS AVAILABLE: "<<std::endl;
    std::cout<<"*****************************"<<std::endl;
    for (auto t : triggers) std::cout<<t<<std::endl;
    std::cout<<"*****************************"<<std::endl;
    std::cout<<"*****************************"<<std::endl;
  }

  m_ftkAnaOut.muonPt.clear();
  m_ftkAnaOut.muonPhi.clear();
  m_ftkAnaOut.muonEta.clear();
  m_ftkAnaOut.muonAuthor.clear();
  m_ftkAnaOut.muonQuality.clear();

  //------------
  // MUONS
  //------------
  // get muon container of interest
  const xAOD::MuonContainer* muons = 0;
  ANA_CHECK(event->retrieve( muons, "HLT_xAOD__MuonContainer_MuonEFInfo" ));
  // loop over the muons in the container
  xAOD::MuonContainer::const_iterator muon_itr = muons->begin();
  xAOD::MuonContainer::const_iterator muon_end = muons->end();
  for( ; muon_itr != muon_end; ++muon_itr ) {
    Info("execute()", "  original muon pt = %.2f GeV", ((*muon_itr)->pt() * 0.001)); // just to print out something
    //add appropriate fields to n-tuple
    if ((*muon_itr)->pt()>25000) {
	    m_ftkAnaOut.muonPt.push_back((*muon_itr)->pt());
	    m_ftkAnaOut.muonPhi.push_back((*muon_itr)->phi());
	    m_ftkAnaOut.muonEta.push_back((*muon_itr)->eta());
	    m_ftkAnaOut.muonAuthor.push_back((*muon_itr)->author());
	    m_ftkAnaOut.muonQuality.push_back((*muon_itr)->quality());
    }
  } // end for loop over muons
  

  // TRIGGER MET
  // HLT_xAOD__TrigMissingETContainer_TrigEFMissingET
  m_ftkAnaOut.trigMet = -9; // this is an error value (no trigger MET was found)
  const xAOD::TrigMissingETContainer* HLT_xAOD__TrigMissingETContainer_TrigEFMissingET = 0;
  //  eventStore()->retrieve( HLT_xAOD__TrigMissingETContainer_TrigEFMissingET, "HLT_xAOD__TrigMissingETContainer_TrigEFMissingET" );
  ANA_CHECK(event->retrieve(HLT_xAOD__TrigMissingETContainer_TrigEFMissingET, "HLT_xAOD__TrigMissingETContainer_TrigEFMissingET"));
  xAOD::TrigMissingETContainer::const_iterator TrigMissingET_iter;
  // std::cout << "Size of TrigMissingETContainer is " << HLT_xAOD__TrigMissingETContainer_TrigEFMissingET->size() << std::endl;
  for(TrigMissingET_iter = HLT_xAOD__TrigMissingETContainer_TrigEFMissingET->begin(); TrigMissingET_iter != HLT_xAOD__TrigMissingETContainer_TrigEFMissingET->end(); ++TrigMissingET_iter) {
    Double_t ex = (*TrigMissingET_iter)->ex();
    Double_t ey = (*TrigMissingET_iter)->ey();
    Double_t et = TMath::Sqrt(ex*ex + ey*ey);
    Double_t ephi = TMath::ATan(ey/ex);
    // std::cout << " missing et = " << et << std::endl;
    m_ftkAnaOut.trigMet = et;
    m_ftkAnaOut.trigMetPhi = ephi;
    break; // this means we will only take the first value, if it exists
  }

  //------------
  // TRIGGER PASS
  //------------
  // examine the HLT_xe80* chains, see if they passed/failed and their total prescale
  auto chainGroup = m_trigDecisionTool->getChainGroup("HLT_xe80.*");
  std::map<std::string,int> triggerCounts;
  for(auto &trig : chainGroup->getListOfTriggers()) {
    auto cg = m_trigDecisionTool->getChainGroup(trig);
    std::string thisTrig = trig;
    if( (m_eventCounter % 100) ==-1 ) //don't print all of this...
      Info( "execute()", "%30s chain passed(1)/failed(0): %d total chain prescale (L1*HLT): %.1f", thisTrig.c_str(), cg->isPassed(), cg->getPrescale() );
  } // end for loop (c++11 style) over chain group matching "HLT_xe80*" 
  //check and store which triggers were passed
  auto hltChainGroup = m_trigDecisionTool->getChainGroup("HLT_xe120_pufit_L1XE50");
  if (hltChainGroup->isPassed()) {
    m_ftkAnaOut.trigPassed_HLT_xe120_pufit_L1XE50 = 1;
  }
  else {
    m_ftkAnaOut.trigPassed_HLT_xe120_pufit_L1XE50 = 0;
  }
  auto l1ChainGroup = m_trigDecisionTool->getChainGroup("L1_XE50");
  if (l1ChainGroup->isPassed()) {
    m_ftkAnaOut.trigPassed_L1_XE50 = 1;
  }
  else {
    m_ftkAnaOut.trigPassed_L1_XE50 = 0;
  }
  auto mu26ChainGroup = m_trigDecisionTool->getChainGroup("HLT_mu26_ivarmedium");
  if (mu26ChainGroup->isPassed()) {
    m_ftkAnaOut.trigPassed_HLT_mu26_ivarmedium = 1;
  }
  else {
    m_ftkAnaOut.trigPassed_HLT_mu26_ivarmedium = 0;
  }
  auto mu50ChainGroup = m_trigDecisionTool->getChainGroup("HLT_mu50");
  if (mu50ChainGroup->isPassed()) {
    m_ftkAnaOut.trigPassed_HLT_mu50= 1;
  }
  else {
    m_ftkAnaOut.trigPassed_HLT_mu50= 0;
  }
  auto e26ChainGroup = m_trigDecisionTool->getChainGroup("HLT_e26_lhmedium_nod0_ivarmedium");
  if (e26ChainGroup->isPassed()) {
    m_ftkAnaOut.trigPassed_HLT_e26_lhmedium_nod0_ivarmedium = 1;
  }
  else {
    m_ftkAnaOut.trigPassed_HLT_e26_lhmedium_nod0_ivarmedium = 0;
  }
  auto j60ChainGroup = m_trigDecisionTool->getChainGroup("HLT_j260");
  if (j60ChainGroup->isPassed()) {
    m_ftkAnaOut.trigPassed_HLT_j60 = 1;
  }
  else {
    m_ftkAnaOut.trigPassed_HLT_j60 = 0;
  }
  auto xe110ChainGroup = m_trigDecisionTool->getChainGroup("HLT_xe110_pufit_L1XE50");
  if (xe110ChainGroup->isPassed()) {
    m_ftkAnaOut.trigPassed_HLT_xe110_pufit_L1XE50 = 1;
  }
  else {
    m_ftkAnaOut.trigPassed_HLT_xe110_pufit_L1XE50 = 0;
  }
  auto e50ChainGroup = m_trigDecisionTool->getChainGroup("HLT_e50_lhvloose_nod0_L1EM15");
  if (e50ChainGroup->isPassed()) {
    m_ftkAnaOut.trigPassed_HLT_e50_lhvloose_nod0_L1EM15 = 1;
  }
  else {
    m_ftkAnaOut.trigPassed_HLT_e50_lhvloose_nod0_L1EM15 = 0;
  }
  auto mu20ChainGroup = m_trigDecisionTool->getChainGroup("L1_MU20");
  if (mu20ChainGroup->isPassed()) {
    m_ftkAnaOut.trigPassed_L1_MU20 = 1;
  }
  else {
    m_ftkAnaOut.trigPassed_L1_MU20 = 0;
  }
  auto mu21ChainGroup = m_trigDecisionTool->getChainGroup("L1_MU21");
  if (mu21ChainGroup->isPassed()) {
    m_ftkAnaOut.trigPassed_L1_MU21 = 1;
  }
  else {
    m_ftkAnaOut.trigPassed_L1_MU21 = 0;
  }
  

  auto cg = m_trigDecisionTool->getChainGroup("HLT_e25_etcut");
  auto fc = cg->features();
  // Container is attached to the navigation
  //auto eleFeatureContainers = fc.containerFeature<TrigElectronContainer>();

  const xAOD::EmTauRoIContainer* l1taus = 0;
  ANA_CHECK(event->retrieve(l1taus, "LVL1EmTauRoIs"));
  const xAOD::JetRoIContainer* l1jets = 0;
  ANA_CHECK(event->retrieve(l1jets, "LVL1JetRoIs"));
  const xAOD::MuonRoIContainer* l1muons = 0;
  ANA_CHECK(event->retrieve(l1muons, "LVL1MuonRoIs"));


  //-------------- to get the number of non-associated tracks -----------------
  const xAOD::TrackParticleContainer* trkContFTK = 0;
  ANA_CHECK(event->retrieve(trkContFTK,  "FTK_TrackParticleContainer" ));

  m_ftkAnaOut.nTracksFTK = trkContFTK->size();

  const xAOD::VertexContainer *vtxContFTK = 0;
  ANA_CHECK(event->retrieve(vtxContFTK,  "FTK_VertexContainer" ));
  
  m_ftkAnaOut.nVtxFTK = vtxContFTK->size();

  //  std::cout << "The number of unassociated tracks are " << getNumberOfNonAssociatedTracks ( trkContFTK, vtxContFTK ) << std::endl;

  m_ftkAnaOut.nNonassociatedTracksFTK = getNumberOfNonAssociatedTracks ( trkContFTK, vtxContFTK );
  getVtxAssociatedTracks ( trkContFTK, vtxContFTK ); //set a bool for each track: isMatched, w/ respect to vertex collection


  //-------------------------
  // FTK Tracks
  //-------------------------
  //loop over FTK tracks
  m_ftkAnaOut.ftkPt.clear();
  m_ftkAnaOut.ftkIsoDR4.clear();
  m_ftkAnaOut.ftkIsoDR2.clear();
  m_ftkAnaOut.ftkZ0.clear();
  m_ftkAnaOut.ftkPhi.clear();
  m_ftkAnaOut.ftkEta.clear();
  m_ftkAnaOut.ftkD0.clear();
  m_ftkAnaOut.ftkCharge.clear();
  m_ftkAnaOut.ftkPixelHits.clear();
  m_ftkAnaOut.ftkPixelHoles.clear();
  m_ftkAnaOut.ftkSCTHits.clear();
  m_ftkAnaOut.ftkSCTHoles.clear();
  m_ftkAnaOut.offPt.clear();
  m_ftkAnaOut.offIsoDR4.clear();
  m_ftkAnaOut.offZ0.clear();
  m_ftkAnaOut.offPhi.clear();
  m_ftkAnaOut.offEta.clear();
  m_ftkAnaOut.offD0.clear();
  m_ftkAnaOut.offCharge.clear();
  m_ftkAnaOut.offPixelHits.clear();
  m_ftkAnaOut.offPixelHoles.clear();
  m_ftkAnaOut.offSCTHits.clear();
  m_ftkAnaOut.offSCTHoles.clear();
  m_ftkAnaOut.isRhadron.clear();
  m_ftkAnaOut.isRhadronDaughter.clear();
  bool foundIsoTrack = false;
  uint8_t sumvalue_holder = 0;
  m_ftkAnaOut.d0sigTrack.clear();
  m_ftkAnaOut.chi2.clear();
  m_ftkAnaOut.nDOF.clear();
  m_ftkAnaOut.rFirstHit.clear();
  m_ftkAnaOut.hasVtxFTK.clear();
  if (vtxContFTK->size() > 0) {
    m_ftkAnaOut.primaryVertexZ = vtxContFTK->at(0)->z();
  }
  for (const auto& ftkTrack : *(trkContFTK)) {
    // loop over ftk tracks
    //if (ftkTrack->pt() < minimum_pt_parameter) {
    //  continue;
    //}
    std::pair<float,float> iso = getFTKTrackIso(*ftkTrack, trkContFTK, vtxContFTK->at(0)->z());
    //if (iso.second/ftkTrack->pt() > 0.1) continue;
    z0pvz_hist->Fill(vtxContFTK->at(0)->z()-(ftkTrack->z0()+ftkTrack->vz()));
    numiblhits = 0;
    ftkTrack->summaryValue(numiblhits, xAOD::numberOfInnermostPixelLayerHits);
    ibl_hits_hist->Fill(numiblhits);
    numiblhits = 0;
    ftkTrack->summaryValue(numiblhits, xAOD::numberOfNextToInnermostPixelLayerHits);
    tbl_hits_hist->Fill(numiblhits);
    sumvalue_holder = 0;
    ftkTrack->summaryValue(sumvalue_holder, xAOD::numberOfPixelHits);
    m_ftkAnaOut.ftkPixelHits.push_back( sumvalue_holder);
    sumvalue_holder = 0;
    ftkTrack->summaryValue(sumvalue_holder, xAOD::numberOfPixelHoles);
    m_ftkAnaOut.ftkPixelHoles.push_back( sumvalue_holder);
    sumvalue_holder = 0;
    ftkTrack->summaryValue(sumvalue_holder, xAOD::numberOfSCTHits);
    m_ftkAnaOut.ftkSCTHits.push_back( sumvalue_holder);
    sumvalue_holder = 0;
    ftkTrack->summaryValue(sumvalue_holder, xAOD::numberOfSCTHoles);
    m_ftkAnaOut.ftkSCTHoles.push_back( sumvalue_holder);

    m_ftkAnaOut.ftkD0.push_back( ftkTrack->d0() );
    m_ftkAnaOut.d0sigTrack.push_back( fabs(ftkTrack->d0())/sqrt( ftkTrack->definingParametersCovMatrix()(0,0)) );
    m_ftkAnaOut.chi2.push_back( ftkTrack->chiSquared() );
    m_ftkAnaOut.nDOF.push_back( ftkTrack->numberDoF() );
    m_ftkAnaOut.rFirstHit.push_back( ftkTrack->radiusOfFirstHit() );
    m_ftkAnaOut.ftkPhi.push_back( ftkTrack->phi() );
    m_ftkAnaOut.ftkEta.push_back( ftkTrack->eta() );
    m_ftkAnaOut.ftkCharge.push_back( ftkTrack->charge());
    m_ftkAnaOut.ftkZ0.push_back( ftkTrack->z0()+ftkTrack->vz() );
    m_ftkAnaOut.hasVtxFTK.push_back( ftkTrack->vertex()==NULL ? false : true  ); //this seems to be always empty
    bool rHadron = false;
    bool rHadronDaughter = false;
   //store FTK track together with FTK isolation
    m_ftkAnaOut.ftkPt.push_back(ftkTrack->pt());
    m_ftkAnaOut.ftkIsoDR2.push_back(iso.first);
    m_ftkAnaOut.ftkIsoDR4.push_back(iso.second);
    if (iso.second/ftkTrack->pt() < isolation_parameter) foundIsoTrack = true;
    float minDeltaEta = 99;
    const xAOD::TruthParticle* closest = 0;
    if (isMC) { // only look for truth in MC
      // TRUTH INFO  
      const xAOD::TruthParticleContainer* truthCont = 0;
      ANA_CHECK(event->retrieve(truthCont, "TruthParticles"));
      //	if (ftkTrack->pt() > 40000) {
	// first see if this matches an R-hadron
	for(const auto& truth : *(truthCont)) {
	  if (isRHadron(*truth)) {
	    if ((getDeltaR(truth,ftkTrack) < deltar_parameter)) rHadron = true;
	  }
	}
	//	} //if trackPt > 40 GeV  -- not sure this cut is at all necessary?
      // if not an Rhadron, see if it's a daughter
      if (!rHadron) {
	for(const auto& truth : *(truthCont)) {
	  float dTheta = fabs ((truth->p4().Theta()-ftkTrack->theta()));
	  if (dTheta < minDeltaEta) {
	    minDeltaEta = dTheta;
	    closest = truth;
	  }
	}//over truth
	if (isRHadronDaughter(*closest)) {
	  if (minDeltaEta < 0.01) rHadronDaughter = true;
	}
      }
    } //if isMC
    m_ftkAnaOut.isRhadron.push_back(rHadron);
    m_ftkAnaOut.isRhadronDaughter.push_back(rHadronDaughter);
  } //ftk tracks
  m_ftkAnaOut.foundIsoTrack = foundIsoTrack;

  //------------------------
  // Offline Track Loop
  //------------------------
  // loop over offline tracks
  const xAOD::TrackParticleContainer* track_particles;
  ANA_CHECK(event->retrieve( track_particles, "InDetTrackParticles"));
  m_ftkAnaOut.offPt.clear();
  for (const xAOD::TrackParticle* tcp : *track_particles) {
    //if (tcp->pt() < minimum_pt_parameter) {
    //  continue;
    //}
    sumvalue_holder = 0;
    tcp->summaryValue(sumvalue_holder, xAOD::numberOfPixelHits);
    m_ftkAnaOut.offPixelHits.push_back( sumvalue_holder);
    sumvalue_holder = 0;
    tcp->summaryValue(sumvalue_holder, xAOD::numberOfPixelHoles);
    m_ftkAnaOut.offPixelHoles.push_back( sumvalue_holder);
    sumvalue_holder = 0;
    tcp->summaryValue(sumvalue_holder, xAOD::numberOfSCTHits);
    m_ftkAnaOut.offSCTHits.push_back( sumvalue_holder);
    sumvalue_holder = 0;
    tcp->summaryValue(sumvalue_holder, xAOD::numberOfSCTHoles);
    m_ftkAnaOut.ftkSCTHoles.push_back( sumvalue_holder);
    m_ftkAnaOut.offD0.push_back(tcp->d0());
    m_ftkAnaOut.offPt.push_back(tcp->pt());
    m_ftkAnaOut.offZ0.push_back(tcp->z0()+tcp->vz());
    m_ftkAnaOut.offPhi.push_back( tcp->phi() );
    m_ftkAnaOut.offEta.push_back( tcp->eta() );
    m_ftkAnaOut.offCharge.push_back( tcp->charge() );
    float dr4_iso = 0;
    //loop over secondary tracks to calculate isolation
    for (const xAOD::TrackParticle* rcp : *track_particles) {
      float delta_phi = (rcp->phi() - tcp->phi());
      if (delta_phi > 3.1415) delta_phi -= 2*3.1415;
      if (delta_phi < -3.1415) delta_phi += 2*3.1415;
      float dR = TMath::Sqrt(TMath::Power(rcp->eta() - tcp->eta(),2) + TMath::Power(delta_phi, 2));
      float theta = tcp->theta();
      float deltazsintheta = TMath::Abs(tcp->z0()-rcp->z0())*TMath::Sin(theta);
      if (deltazsintheta < 15 && dR < 0.4 && tcp != rcp) {
        dr4_iso += rcp->pt();
      }
    }
    m_ftkAnaOut.offIsoDR4.push_back(dr4_iso);
  }

  //----------------------
  // Truth Tracks
  //----------------------
  // find the number of charged R hadrons and the number of charged R hadrons with matching ftk tracks
  m_ftkAnaOut.nChargedRHadron = 0;
  m_ftkAnaOut.nChargedRHadronFTKMatched = 0;
  // loop over truth particles
  // only if is simulation
  if (isMC) {
    const xAOD::TruthParticleContainer* truthCont = 0;
    ANA_CHECK(event->retrieve(truthCont, "TruthParticles"));
    for(const auto& truth : *(truthCont)) {
      // check if the truth particle is a charged R Hadron
      if (isChargedRHadron(*truth) && fabs(truth->eta()) < 2.5) {
	m_ftkAnaOut.nChargedRHadron++;
	m_ftkAnaOut.chargedRHadronPt.push_back(truth->pt());
	m_ftkAnaOut.chargedRHadronDecayRadius.push_back(getDecayRadius(*truth));
	// loop over ftk tracks
	bool foundMatchingFTKTrack = 0;
        //const xAOD::TrackParticleContainer* trkContFTK = 0;
        //ANA_CHECK(event->retrieve(trkContFTK,  "FTK_TrackParticleContainer" ));
	for (const auto& ftkTrack : *(trkContFTK)) {
	  // check if an ftk track matches the charged R hadron - if it does, fill some branches and break from this loop
	  if ((getDeltaR(truth,ftkTrack) < 0.01)) {
	    m_ftkAnaOut.nChargedRHadronFTKMatched++;
	    m_ftkAnaOut.chargedRHadronFTKMatched.push_back(1);
	    m_ftkAnaOut.chargedRHadronFTKPt.push_back(ftkTrack->pt());
	    foundMatchingFTKTrack = 1;
	    break;
	  }
	}
	// if no ftk track was found, fill these branches with null/error/default values
	if (!foundMatchingFTKTrack) {
	  m_ftkAnaOut.chargedRHadronFTKMatched.push_back(0);
	  m_ftkAnaOut.chargedRHadronFTKPt.push_back(-9);
	}
      }
    }
  }

  //----------------------------------------------------------------


  m_outTree->Fill();

  return EL::StatusCode::SUCCESS;
}



int MyTrigAnalysis :: getNumberOfNonAssociatedTracks ( const xAOD::TrackParticleContainer* trkCont, const xAOD::VertexContainer *vtxCont)
{


  // A sanity check:
  if( ! vtxCont->size() ) {
    Warning( "execute()", "Event with no (FTK) vertex found!" );
    return -1;
  }
  

  xAOD::TrackVertexAssociationMap trktovtxmap =
    m_trktovtxtool->getUniqueMatchMap( *trkCont, *vtxCont );

  if( (m_eventCounter % 100) ==0 ) //don't print all of this...
    Info( "execute()", "Size of TrackVertexAssociationMap for *the* primary vertex: %lu", trktovtxmap[ vtxCont->at( 0 ) ].size() );

  int nTrks=0;

  //loop over the tracks:
  for (const auto& trk_itr : *trkCont ) {
    
    ElementLink< xAOD::VertexContainer > match_vtx;
    
    //get the matching vtx
    match_vtx = m_trktovtxtool->getUniqueMatchVertexLink( *trk_itr,  *vtxCont );
    
    
    /*
    if( match_vtx.isValid() ) {
      Info( "execute()", "Vertex assigned to the FTK track particle:" );
      std::cout << match_vtx << std::endl;
      std::cout << *match_vtx << std::endl;
      std::cout << "at z position " << ( *match_vtx )->z() << std::endl;

    }
    */


    if( !match_vtx.isValid() ) //    else
      nTrks++;
    
  }

  return nTrks;

}



EL::StatusCode MyTrigAnalysis :: getVtxAssociatedTracks ( const xAOD::TrackParticleContainer* trkCont, const xAOD::VertexContainer *vtxCont)
{


  // A sanity check:
  if( ! vtxCont->size() ) {
    Warning( "execute()", "Event with no (FTK) vertex found!" );
    return EL::StatusCode::FAILURE;

  }  

  m_ftkAnaOut.isVtxMatched.clear();

  xAOD::TrackVertexAssociationMap trktovtxmap =
    m_trktovtxtool->getUniqueMatchMap( *trkCont, *vtxCont );

  if( (m_eventCounter % 100) ==0 ) //don't print all of this...
    Info( "execute()", "Size of TrackVertexAssociationMap for *the* primary vertex: %lu", trktovtxmap[ vtxCont->at( 0 ) ].size() );


  //loop over the tracks:
  for (const auto& trk_itr : *trkCont ) {
    
    ElementLink< xAOD::VertexContainer > match_vtx;
    
    //get the matching vtx
    match_vtx = m_trktovtxtool->getUniqueMatchVertexLink( *trk_itr,  *vtxCont );
    
    m_ftkAnaOut.isVtxMatched.push_back( match_vtx.isValid() );
    

  }

  return EL::StatusCode::SUCCESS;

}


EL::StatusCode MyTrigAnalysis :: postExecute ()
{
  // Here you do everything that needs to be done after the main event
  // processing.  This is typically very rare, particularly in user
  // code.  It is mainly used in implementing the NTupleSvc.
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode MyTrigAnalysis :: finalize ()
{
  // This method is the mirror image of initialize(), meaning it gets
  // called after the last event has been processed on the worker node
  // and allows you to finish up any objects you created in
  // initialize() before they are written to disk.  This is actually
  // fairly rare, since this happens separately for each worker node.
  // Most of the time you want to do your post-processing on the
  // submission node after all your histogram outputs have been
  // merged.  This is different from histFinalize() in that it only
  // gets called on worker nodes that processed input events.

  ANA_CHECK_SET_TYPE (EL::StatusCode); // set type of return code you are expecting (add to top of each function once)

  // cleaning up trigger tools
  if( m_trigConfigTool ) {
      delete m_trigConfigTool;
      m_trigConfigTool = 0;
  }
  if( m_trigDecisionTool ) {
      delete m_trigDecisionTool;
      m_trigDecisionTool = 0;
  }
   
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode MyTrigAnalysis :: histFinalize ()
{
  // This method is the mirror image of histInitialize(), meaning it
  // gets called after the last event has been processed on the worker
  // node and allows you to finish up any objects you created in
  // histInitialize() before they are written to disk.  This is
  // actually fairly rare, since this happens separately for each
  // worker node.  Most of the time you want to do your
  // post-processing on the submission node after all your histogram
  // outputs have been merged.  This is different from finalize() in
  // that it gets called on all worker nodes regardless of whether
  // they processed input events.
  return EL::StatusCode::SUCCESS;
}

std::pair<float,float> MyTrigAnalysis::getFTKTrackIso(const xAOD::TrackParticle& ftkTrack, const xAOD::TrackParticleContainer *trkContFTK, float primary_vertex_z)
{
  //loop over FTK tracks and select the ones that match a fixed cone and are not ftkTrack
  std::pair<float,float> iso;
  iso.first = 0.0;
  iso.second = 0.0;
  for (const auto& itFtkTrack : *(trkContFTK)) {
    float delta_phi = (ftkTrack.phi() - itFtkTrack->phi());
    if (delta_phi > 3.1415) delta_phi -= 2*3.1415;
    if (delta_phi < -3.1415) delta_phi += 2*3.1415;
    float dR = TMath::Sqrt(TMath::Power(ftkTrack.eta() - itFtkTrack->eta(),2) + TMath::Power(delta_phi, 2));
    //float ztrack = (itFtkTrack->z0()+itFtkTrack->vz());
    //float deltaz = ztrack-primary_vertex_z;
    float theta = itFtkTrack->theta();
    float deltazsintheta = TMath::Abs(itFtkTrack->z0()-ftkTrack.z0())*TMath::Sin(theta);
    if (deltazsintheta < 15) {
      if (dR < 0.4) {
        iso.second += itFtkTrack->pt(); //DR=0.4
        if (dR < 0.2) {
  	  iso.first += itFtkTrack->pt();//DR=0.2
        }
      }
    }
  }  
  //remove original track from isolation computation
  iso.first = iso.first - ftkTrack.pt();
  iso.second = iso.second - ftkTrack.pt();
  return iso;
}

