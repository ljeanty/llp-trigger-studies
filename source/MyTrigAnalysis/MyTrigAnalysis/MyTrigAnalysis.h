#ifndef MyTrigAnalysis_MyTrigAnalysis_H
#define MyTrigAnalysis_MyTrigAnalysis_H

#include <EventLoop/Algorithm.h>

// include files for using the trigger tools
#include <TrigConfxAOD/xAODConfigTool.h>
#include <TrigDecisionTool/TrigDecisionTool.h>
#include <xAODTracking/TrackParticleContainer.h>

#include "MyTrigAnalysis/MyFTKAnaOut.h"

#include <TTree.h>

#include <memory>

// include histogram tools
#include <TH1.h>

// Track/vertex matching tools:
#include "TrackVertexAssociationTool/LooseTrackVertexAssociationTool.h"
#include "TrackVertexAssociationTool/TightTrackVertexAssociationTool.h"
#include "TrackVertexAssociationTool/BaseTrackVertexAssociationTool.h"


class MyTrigAnalysis : public EL::Algorithm
{
  // put your configuration variables here as public variables.
  // that way they can be set directly from CINT and python.
public:
  // float cutValue;

  std::string outputName;

  //output
  std::shared_ptr<TTree> m_outTree; //!
  MyFTKAnaOut m_ftkAnaOut; //!

  int m_eventCounter; //!

  // trigger tools member variables
  Trig::TrigDecisionTool *m_trigDecisionTool; //!
  TrigConf::xAODConfigTool *m_trigConfigTool; //!

  //settings
  float isolation_parameter = 0.1; //!
  float deltar_parameter = 0.01; //!
  float minimum_pt_parameter = 0; //!
  //20000

  //track/vtx association tool 
  CP::BaseTrackVertexAssociationTool * m_trktovtxtool; //!

  // variables that don't get filled at submission time should be
  // protected from being send from the submission node to the worker
  // node (done by the //!)
public:
  // Tree *myTree; //!
  // TH1 *myHist; //!
  TH1F *z0pvz_hist; //!
  TH1I *ibl_hits_hist; //!
  TH1I *tbl_hits_hist; //!
  uint8_t numiblhits; //!

  // this is a standard constructor
  MyTrigAnalysis ();

  // these are the functions inherited from Algorithm
  virtual EL::StatusCode setupJob (EL::Job& job);
  virtual EL::StatusCode fileExecute ();
  virtual EL::StatusCode histInitialize ();
  virtual EL::StatusCode changeInput (bool firstFile);
  virtual EL::StatusCode initialize ();
  virtual EL::StatusCode execute ();
  virtual EL::StatusCode postExecute ();
  virtual EL::StatusCode finalize ();
  virtual EL::StatusCode histFinalize ();


  //track-to-vertex (non)-association fct
  int getNumberOfNonAssociatedTracks ( const xAOD::TrackParticleContainer* trkCont, const xAOD::VertexContainer *vtxCont);
  virtual EL::StatusCode getVtxAssociatedTracks ( const xAOD::TrackParticleContainer* trkCont, const xAOD::VertexContainer *vtxCont);

  bool truthInfo (const xAOD::TrackParticle& track);

  ///Return DR=0.2,0.4 isolation for the given FTK track
  std::pair<float,float> getFTKTrackIso(const xAOD::TrackParticle& ftkTrack, const xAOD::TrackParticleContainer *trkContFTK, float primary_vertex_z);

  // this is needed to distribute the algorithm to the workers
  ClassDef(MyTrigAnalysis, 1);
};

#endif
