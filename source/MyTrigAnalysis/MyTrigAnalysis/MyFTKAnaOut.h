#ifndef MyTrigAnalysis_MyFTKAnaOut_H
#define MyTrigAnalysis_MyFTKAnaOut_H
#include <vector>
#include <memory>

#include <TTree.h>

struct MyFTKAnaOut {
  int runNumber;
  int eventNumber;

  float mu;
  int nTracksFTK;
  int nVtxFTK;
  int nChargedRHadron;
  int nChargedRHadronFTKMatched;
  std::vector<float>chargedRHadronPt;
  std::vector<float>chargedRHadronDecayRadius;
  std::vector<bool>chargedRHadronFTKMatched;
  std::vector<float>chargedRHadronFTKPt;
  std::vector<float> d0sigTrack;

  float trigMet;
  float trigMetPhi;
  bool trigPassed_HLT_xe120_pufit_L1XE50;
  bool trigPassed_L1_XE50;
  bool trigPassed_HLT_mu26_ivarmedium;
  bool trigPassed_HLT_mu50;
  bool trigPassed_HLT_e26_lhmedium_nod0_ivarmedium;
  bool trigPassed_HLT_j60;
  bool trigPassed_HLT_xe110_pufit_L1XE50;
  bool trigPassed_L1_MU20;
  bool trigPassed_L1_MU21;
  bool trigPassed_HLT_e50_lhvloose_nod0_L1EM15;
  int nNonassociatedTracksFTK;
  bool foundIsoTrack;
  float primaryVertexZ;

  std::vector<float> ftkPt;
  std::vector<float> ftkIsoDR2;
  std::vector<float> ftkIsoDR4;
  std::vector<float> ftkZ0;
  std::vector<float> ftkPhi;
  std::vector<float> ftkEta;
  std::vector<float> ftkD0;
  std::vector<float> ftkCharge;
  std::vector<uint8_t> ftkPixelHits;
  std::vector<uint8_t> ftkPixelHoles;
  std::vector<uint8_t> ftkSCTHits;
  std::vector<uint8_t> ftkSCTHoles;
  std::vector<float> isRhadron;
  std::vector<bool> isRhadronDaughter;
  std::vector<bool> isVtxMatched;
  std::vector<bool> hasVtxFTK;
  std::vector<float> chi2;
  std::vector<float> nDOF;
  std::vector<float> rFirstHit;
  std::vector<float> offZ0;
  std::vector<float> offPt;
  std::vector<float> offIsoDR4;
  std::vector<float> offPhi;
  std::vector<float> offEta;
  std::vector<float> offD0;
  std::vector<float> offCharge; 
  std::vector<uint8_t> offPixelHits;
  std::vector<uint8_t> offPixelHoles;
  std::vector<uint8_t> offSCTHits;
  std::vector<uint8_t> offSCTHoles;
  std::vector<float> muonPt;
  std::vector<float> muonPhi;
  std::vector<float> muonEta;
  std::vector<uint16_t> muonAuthor;
  std::vector<uint8_t> muonQuality;

  std::shared_ptr<TTree> getTree(const char* tName, const char* tTitle) {
    auto outTree = std::make_shared<TTree> (tName, tTitle);
    outTree->Branch("runNumber", &runNumber);
    outTree->Branch("eventNumber", &eventNumber);
    outTree->Branch("mu", &mu);
    outTree->Branch("nTracksFTK", &nTracksFTK);
    outTree->Branch("nVtxFTK", &nVtxFTK);
    outTree->Branch("nChargedRHadron", &nChargedRHadron);
    outTree->Branch("nChargedRHadronFTKMatched", &nChargedRHadronFTKMatched);
    outTree->Branch("chargedRHadronPt", &chargedRHadronPt);
    outTree->Branch("chargedRHadronDecayRadius", &chargedRHadronDecayRadius);
    outTree->Branch("chargedRHadronFTKMatched", &chargedRHadronFTKMatched);
    outTree->Branch("chargedRHadronFTKPt", &chargedRHadronFTKPt);
    outTree->Branch("nNonassociatedTracksFTK", &nNonassociatedTracksFTK);
    outTree->Branch("d0sigTrack", &d0sigTrack);
    outTree->Branch("trigMet", &trigMet);
    outTree->Branch("trigMetPhi", &trigMetPhi);
    outTree->Branch("foundIsoTrack", &foundIsoTrack);
    outTree->Branch("trigPassed_HLT_xe120_pufit_L1XE50", &trigPassed_HLT_xe120_pufit_L1XE50);
    outTree->Branch("trigPassed_L1_XE50", &trigPassed_L1_XE50);
    outTree->Branch("ftkPt", &ftkPt);
    outTree->Branch("ftkPhi", &ftkPhi);
    outTree->Branch("ftkEta", &ftkEta);
    outTree->Branch("ftkIsoDR4", &ftkIsoDR4); //thing that stores isolation
    outTree->Branch("ftkIsoDR2", &ftkIsoDR2);
    outTree->Branch("ftkD0", &ftkD0);
    outTree->Branch("ftkPixelHits", &ftkPixelHits);
    outTree->Branch("ftkPixelHoles", &ftkPixelHoles);
    outTree->Branch("ftkSCTHits", &ftkSCTHits);
    outTree->Branch("ftkSCTHoles", &ftkSCTHoles);
    outTree->Branch("isRhadron", &isRhadron);
    outTree->Branch("isRhadronDaughter", &isRhadronDaughter);
    outTree->Branch("isVtxMatched", &isVtxMatched);
    outTree->Branch("hasVtxFTK", &hasVtxFTK);
    outTree->Branch("chi2", &chi2);
    outTree->Branch("nDOF", &nDOF);
    outTree->Branch("rFirstHit", &rFirstHit);
    outTree->Branch("ftkZ0", &ftkZ0);
    outTree->Branch("primaryVertexZ", &primaryVertexZ);
    outTree->Branch("trigPassed_HLT_mu26_ivarmedium", &trigPassed_HLT_mu26_ivarmedium);
    outTree->Branch("trigPassed_HLT_mu50", &trigPassed_HLT_mu50);
    outTree->Branch("trigPassed_HLT_e26_lhmedium_nod0_ivarmedium", &trigPassed_HLT_e26_lhmedium_nod0_ivarmedium);
    outTree->Branch("trigPassed_HLT_j60", &trigPassed_HLT_j60);
    outTree->Branch("trigPassed_HLT_xe110_pufit_L1XE50", &trigPassed_HLT_xe110_pufit_L1XE50);
    outTree->Branch("trigPassed_HLT_e50_lhvloose_nod0_L1EM15", &trigPassed_HLT_e50_lhvloose_nod0_L1EM15);
    outTree->Branch("trigPassed_L1_MU20", &trigPassed_L1_MU20);
    outTree->Branch("trigPassed_L1_MU21", &trigPassed_L1_MU21);
    outTree->Branch("offPt", &offPt);
    outTree->Branch("offPhi", &offPhi);
    outTree->Branch("offEta", &offEta);
    outTree->Branch("offIsoDR4", &offIsoDR4);
    outTree->Branch("offZ0", &offZ0);
    outTree->Branch("ftkCharge", &ftkCharge);
    outTree->Branch("offCharge", &offCharge);
    outTree->Branch("offD0", &offD0);
    outTree->Branch("offPixelHits", &offPixelHits);
    outTree->Branch("offPixelHoles", &offPixelHoles);
    outTree->Branch("offSCTHits", &offSCTHits);
    outTree->Branch("offSCTHoles", &offSCTHoles);
    outTree->Branch("muonPt", &muonPt);
    outTree->Branch("muonPhi", &muonPhi);
    outTree->Branch("muonEta", &muonEta);
    outTree->Branch("muonAuthor", &muonAuthor);
    outTree->Branch("muonQuality", &muonQuality);
    return outTree;
  }

};

#endif // MyTrigAnalysis_MyFTKAnaOut_H
