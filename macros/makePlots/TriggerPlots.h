//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Fri Apr 28 09:23:16 2017 by ROOT version 6.04/16
// from TTree tree/tree
// found on file: ../../large/data-ftkAnaOutput/haddedTrees.root
//////////////////////////////////////////////////////////

#ifndef TriggerPlots_h
#define TriggerPlots_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>

// Header file for the classes stored in the TTree if any.
#include "vector"
#include "vector"

class TriggerPlots {
public :
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain

// Fixed size dimensions of array or collections stored in the TTree if any.

   // Declaration of leaf types
   Int_t           runNumber;
   Int_t           eventNumber;
   Float_t         mu;
   Int_t           nTracksFTK;
   Int_t           nVtxFTK;
   Int_t           nNonassociatedTracksFTK;
   vector<float>   *d0sigTrack;
   Float_t         trigMet;
   Float_t         trigMetPhi;
   Float_t	   primaryVertexZ;
   Bool_t          foundIsoTrack;
   Bool_t          trigPassed_HLT_xe120_pufit_L1XE50;
   Bool_t          trigPassed_L1_XE50;
   Bool_t	   trigPassed_HLT_mu26_ivarmedium;
   Bool_t	   trigPassed_HLT_mu50;
   Bool_t	   trigPassed_HLT_e26_lhmedium_nod0_ivarmedium;
   Bool_t	   trigPassed_HLT_j60;
   Bool_t          trigPassed_HLT_xe110_pufit_L1XE50;
   Bool_t          trigPassed_L1_MU20;
   Bool_t          trigPassed_L1_MU21;
   Bool_t	   trigPassed_HLT_e50_lhvloose_nod0_L1EM15;
   vector<float>   *ftkPt;
   vector<float>   *ftkZ0;
   vector<float>   *ftkIsoDR4;
   vector<float>   *ftkIsoDR2;
   vector<float>   *isRhadron;
   vector<bool>    *isRhadronDaughter;
   vector<bool>    *isVtxMatched;
   vector<bool>    *hasVtxFTK;
   vector<float>   *chi2;
   vector<float>   *nDOF;
   vector<float>   *rFirstHit;
   vector<float>   *offPt;
   vector<float>   *offIsoDR4;
   vector<float>   *offZ0;
   vector<float>   *offEta;
   vector<float>   *offPhi;
   vector<float>   *ftkEta;
   vector<float>   *ftkPhi;
   vector<float>   *ftkCharge;
   vector<float>   *offCharge;
   vector<float>   *ftkD0;
   vector<uint8_t>   *ftkPixelHits;
   vector<uint8_t>   *ftkPixelHoles;
   vector<uint8_t>   *ftkSCTHits;
   vector<uint8_t>   *ftkSCTHoles;
   vector<float>   *offD0;
   vector<uint8_t>   *offPixelHits;
   vector<uint8_t>   *offPixelHoles;
   vector<uint8_t>   *offSCTHits;
   vector<uint8_t>   *offSCTHoles;
   vector<uint8_t>	*muonQuality;
   vector<uint16_t>	*muonAuthor;
   vector<float>	*muonPt;
   vector<float>	*muonPhi;
   vector<float>	*muonEta;

   // List of branches
   TBranch        *b_runNumber;   //!
   TBranch        *b_eventNumber;   //!
   TBranch        *b_mu;   //!
   TBranch	  *b_primaryVertexZ; //!
   TBranch        *b_nTracksFTK;   //!
   TBranch        *b_nVtxFTK;   //!
   TBranch        *b_nNonassociatedTracksFTK;   //!
   TBranch        *b_d0sigTrack;   //!
   TBranch        *b_trigMet;   //!
   TBranch        *b_trigMetPhi;   //!
   TBranch        *b_foundIsoTrack;   //!
   TBranch        *b_trigPassed_HLT_xe120_pufit_L1XE50;   //!
   TBranch        *b_trigPassed_L1_XE50;   //!
   TBranch        *b_trigPassed_HLT_mu26_ivarmedium;   //!
   TBranch        *b_trigPassed_HLT_mu50;   //!
   TBranch        *b_trigPassed_HLT_e26_lhmedium_nod0_ivarmedium;   //!
   TBranch        *b_trigPassed_HLT_j60;   //!
   TBranch        *b_trigPassed_HLT_xe110_pufit_L1XE50;   //!
   TBranch        *b_trigPassed_L1_MU20;   //!
   TBranch        *b_trigPassed_L1_MU21;   //!
   TBranch	  *b_trigPassed_HLT_e50_lhvloose_nod0_L1EM15; //!
   TBranch        *b_ftkPt;   //!
   TBranch        *b_ftkZ0;   //!
   TBranch        *b_ftkIsoDR4;   //!
   TBranch        *b_ftkIsoDR2;   //!
   TBranch        *b_isRhadron;   //!
   TBranch        *b_isRhadronDaughter;   //!
   TBranch        *b_isVtxMatched;   //!
   TBranch        *b_hasVtxFTK;   //!
   TBranch        *b_chi2;   //!
   TBranch        *b_nDOF;   //!
   TBranch        *b_rFirstHit;   //!
   TBranch	  *b_offPt; //!
   TBranch	  *b_offIsoDR4; //!
   TBranch	  *b_offZ0; //!
   TBranch	  *b_offPhi; //!
   TBranch	  *b_offEta; //!
   TBranch	  *b_ftkPhi; //!
   TBranch	  *b_ftkEta; //!
   TBranch	  *b_ftkCharge; //!
   TBranch	  *b_offCharge; //!
   TBranch 	  *b_ftkD0; //!
   TBranch 	  *b_ftkPixelHits; //!
   TBranch 	  *b_ftkPixelHoles; //!
   TBranch 	  *b_ftkSCTHits; //!
   TBranch 	  *b_ftkSCTHoles; //!
   TBranch 	  *b_offD0; //!
   TBranch 	  *b_offPixelHits; //!
   TBranch 	  *b_offPixelHoles; //!
   TBranch 	  *b_offSCTHits; //!
   TBranch 	  *b_offSCTHoles; //!
   TBranch	  *b_muonQuality; //!
   TBranch	  *b_muonAuthor; //!
   TBranch	  *b_muonPt; //!
   TBranch	  *b_muonPhi; //!
   TBranch	  *b_muonEta; //!

   TriggerPlots(TTree *tree=0);
   virtual ~TriggerPlots();
   virtual Int_t    Cut(Long64_t entry);
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Long64_t LoadTree(Long64_t entry);
   virtual void     Init(TTree *tree);
   virtual void     Loop();
   virtual Bool_t   Notify();
   virtual void     Show(Long64_t entry = -1);
};

#endif

#ifdef TriggerPlots_cxx
TriggerPlots::TriggerPlots(TTree *tree) : fChain(0) 
{
// if parameter tree is not specified (or zero), connect the file
// used to generate this class and read the Tree.
   if (tree == 0) {
      TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject("../large/data-ftkAnaOutput/haddedTrees.root");
      //TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject("../../run/submitDir/data-ftkAnaOutput/data17_13TeV.00327265.physics_EnhancedBias.merge.AOD.r9949_r10021_r10018_p3313.root");
      if (!f || !f->IsOpen()) {
         f = new TFile("../large/data-ftkAnaOutput/haddedTrees.root");
         //f = new TFile("../../run/submitDir/data-ftkAnaOutput/data17_13TeV.00327265.physics_EnhancedBias.merge.AOD.r9949_r10021_r10018_p3313.root");
      }
      f->GetObject("tree",tree);

   }
   Init(tree);
}

TriggerPlots::~TriggerPlots()
{
   if (!fChain) return;
   delete fChain->GetCurrentFile();
}

Int_t TriggerPlots::GetEntry(Long64_t entry)
{
// Read contents of entry.
   if (!fChain) return 0;
   return fChain->GetEntry(entry);
}
Long64_t TriggerPlots::LoadTree(Long64_t entry)
{
// Set the environment to read one entry
   if (!fChain) return -5;
   Long64_t centry = fChain->LoadTree(entry);
   if (centry < 0) return centry;
   if (fChain->GetTreeNumber() != fCurrent) {
      fCurrent = fChain->GetTreeNumber();
      Notify();
   }
   return centry;
}

void TriggerPlots::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   // Set object pointer
   //d0Track = 0;
   d0sigTrack = 0;
   ftkPt = 0;
   ftkIsoDR4 = 0;
   ftkIsoDR2 = 0;
   isRhadron = 0;
   isRhadronDaughter = 0;
   isVtxMatched = 0;
   hasVtxFTK = 0;
   chi2 = 0;
   nDOF = 0;
   rFirstHit = 0;
   offPt = 0;
   offIsoDR4 = 0;
   offZ0 = 0;
   offPhi = 0;
   offEta = 0;
   ftkPhi = 0;
   ftkEta = 0;
   ftkCharge = 0;
   offCharge = 0;
   ftkD0 = 0;
   ftkPixelHits = 0;
   ftkPixelHoles = 0;
   ftkSCTHits = 0;
   ftkSCTHoles = 0;
   offD0 = 0;
   offPixelHits = 0;
   offPixelHoles = 0;
   offSCTHits = 0;
   offSCTHoles = 0;
   trigPassed_HLT_e50_lhvloose_nod0_L1EM15 = false;
   trigMetPhi = 0; 
   muonQuality = 0;
   muonAuthor = 0;
   muonPt = 0;
   muonPhi = 0;
   muonEta = 0;
   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fCurrent = -1;
   fChain->SetMakeClass(1);

   fChain->SetBranchAddress("runNumber", &runNumber, &b_runNumber);
   fChain->SetBranchAddress("eventNumber", &eventNumber, &b_eventNumber);
   fChain->SetBranchAddress("mu", &mu, &b_mu);
   fChain->SetBranchAddress("primaryVertexZ", &primaryVertexZ, &b_primaryVertexZ);
   fChain->SetBranchAddress("nTracksFTK", &nTracksFTK, &b_nTracksFTK);
   fChain->SetBranchAddress("nVtxFTK", &nVtxFTK, &b_nVtxFTK);
   fChain->SetBranchAddress("nNonassociatedTracksFTK", &nNonassociatedTracksFTK, &b_nNonassociatedTracksFTK);
   fChain->SetBranchAddress("d0sigTrack", &d0sigTrack, &b_d0sigTrack);
   fChain->SetBranchAddress("trigMet", &trigMet, &b_trigMet);
   fChain->SetBranchAddress("trigMetPhi", &trigMetPhi, &b_trigMetPhi);
   fChain->SetBranchAddress("foundIsoTrack", &foundIsoTrack, &b_foundIsoTrack);
   fChain->SetBranchAddress("trigPassed_HLT_xe120_pufit_L1XE50", &trigPassed_HLT_xe120_pufit_L1XE50, &b_trigPassed_HLT_xe120_pufit_L1XE50);
   fChain->SetBranchAddress("trigPassed_L1_XE50", &trigPassed_L1_XE50, &b_trigPassed_L1_XE50);
   fChain->SetBranchAddress("trigPassed_HLT_mu26_ivarmedium", &trigPassed_HLT_mu26_ivarmedium, &b_trigPassed_HLT_mu26_ivarmedium);
   fChain->SetBranchAddress("trigPassed_HLT_mu50", &trigPassed_HLT_mu50, &b_trigPassed_HLT_mu50);
   fChain->SetBranchAddress("trigPassed_HLT_e26_lhmedium_nod0_ivarmedium", &trigPassed_HLT_e26_lhmedium_nod0_ivarmedium, &b_trigPassed_HLT_e26_lhmedium_nod0_ivarmedium);
   fChain->SetBranchAddress("trigPassed_HLT_j60", &trigPassed_HLT_j60, &b_trigPassed_HLT_j60);
   fChain->SetBranchAddress("trigPassed_HLT_xe110_pufit_L1XE50", &trigPassed_HLT_xe110_pufit_L1XE50, &b_trigPassed_HLT_xe110_pufit_L1XE50);
   fChain->SetBranchAddress("trigPassed_L1_MU20", &trigPassed_L1_MU20, &b_trigPassed_L1_MU20);
   fChain->SetBranchAddress("trigPassed_L1_MU21", &trigPassed_L1_MU21, &b_trigPassed_L1_MU21);
   fChain->SetBranchAddress("trigPassed_HLT_e50_lhvloose_nod0_L1EM15", &trigPassed_HLT_e50_lhvloose_nod0_L1EM15, &b_trigPassed_HLT_e50_lhvloose_nod0_L1EM15);
   fChain->SetBranchAddress("ftkPt", &ftkPt, &b_ftkPt);
   fChain->SetBranchAddress("ftkZ0", &ftkZ0, &b_ftkZ0);
   fChain->SetBranchAddress("ftkIsoDR4", &ftkIsoDR4, &b_ftkIsoDR4);
   fChain->SetBranchAddress("ftkIsoDR2", &ftkIsoDR2, &b_ftkIsoDR2);
   fChain->SetBranchAddress("isRhadron", &isRhadron, &b_isRhadron);
   fChain->SetBranchAddress("isRhadronDaughter", &isRhadronDaughter, &b_isRhadronDaughter);
   fChain->SetBranchAddress("isVtxMatched", &isVtxMatched, &b_isVtxMatched);
   fChain->SetBranchAddress("hasVtxFTK", &hasVtxFTK, &b_hasVtxFTK);
   fChain->SetBranchAddress("chi2", &chi2, &b_chi2);
   fChain->SetBranchAddress("nDOF", &nDOF, &b_nDOF);
   fChain->SetBranchAddress("rFirstHit", &rFirstHit, &b_rFirstHit);
   fChain->SetBranchAddress("offPt", &offPt, &b_offPt);
   fChain->SetBranchAddress("offIsoDR4", &offIsoDR4, &b_offIsoDR4);
   fChain->SetBranchAddress("offZ0", &offZ0, &b_offZ0);
   fChain->SetBranchAddress("offPhi", &offPhi, &b_offPhi);
   fChain->SetBranchAddress("offEta", &offEta, &b_offEta);
   fChain->SetBranchAddress("ftkPhi", &ftkPhi, &b_ftkPhi);
   fChain->SetBranchAddress("ftkEta", &ftkEta, &b_ftkEta);
   fChain->SetBranchAddress("ftkCharge", &ftkCharge, &b_ftkCharge);
   fChain->SetBranchAddress("offCharge", &offCharge, &b_offCharge);
   fChain->SetBranchAddress("ftkD0", &ftkD0, &b_ftkD0);
   fChain->SetBranchAddress("ftkPixelHits", &ftkPixelHits, &b_ftkPixelHits);
   fChain->SetBranchAddress("ftkPixelHoles", &ftkPixelHoles, &b_ftkPixelHoles);
   fChain->SetBranchAddress("ftkSCTHits", &ftkSCTHits, &b_ftkSCTHits);
   fChain->SetBranchAddress("ftkSCTHoles", &ftkSCTHoles, &b_ftkSCTHoles);
   fChain->SetBranchAddress("offD0", &offD0, &b_offD0);
   fChain->SetBranchAddress("offPixelHits", &offPixelHits, &b_offPixelHits);
   fChain->SetBranchAddress("offPixelHoles", &offPixelHoles, &b_offPixelHoles);
   fChain->SetBranchAddress("offSCTHits", &offSCTHits, &b_offSCTHits);
   fChain->SetBranchAddress("offSCTHoles", &offSCTHoles, &b_offSCTHoles);
   fChain->SetBranchAddress("muonQuality", &muonQuality, &b_muonQuality);
   fChain->SetBranchAddress("muonAuthor", &muonAuthor, &b_muonAuthor);
   fChain->SetBranchAddress("muonPt", &muonPt, &b_muonPt);
   fChain->SetBranchAddress("muonPhi", &muonPhi, &b_muonPhi);
   fChain->SetBranchAddress("muonEta", &muonEta, &b_muonEta);
   Notify();
}

Bool_t TriggerPlots::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

void TriggerPlots::Show(Long64_t entry)
{
// Print contents of entry.
// If entry is not specified, print current entry
   if (!fChain) return;
   fChain->Show(entry);
}
Int_t TriggerPlots::Cut(Long64_t entry)
{
// This function may be called from Loop.
// returns  1 if entry is accepted.
// returns -1 otherwise.
   return 1;
}
#endif // #ifdef TriggerPlots_cxx
