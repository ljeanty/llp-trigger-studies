#include "TriggerPlots.C"

#include <map>
#include <string>
#include <vector>

#include "TH1.h"
#include "TH1F.h"
#include "TChain.h"
#include "TProfile.h"

using namespace std;

class awesomeTrack : public TriggerPlots {
 public:
  awesomeTrack(std::string fileName);

  awesomeTrack(TTree *tree);

  ~awesomeTrack();

  /// The only function needing implementation                                                                                                
  virtual void     Loop();

  /// Write all histograms                                                                                                                    
  void writeAllHists();

  /// Draw final plots                                                                                                                        
  Int_t drawPlots(string outputFile="awesomeTrack.root");

 protected:

  ///Init histograms                                                                                                                          
  void InitHists();

  map<string, TH1*> allHists;
  //map<string, TH1*> ptHists;
  TH2F* pt_pt_hist; //!
  TH1F* ftk_pt_hist; //!
  TH1F* off_pt_hist; //!
  TH2F* delta_hits_hist; //!
  TH1F* highpt_eta_hist; //!
  TH1F* muon_eta_hist; //!
  TH1F* off_noiso_pt_hist; //!
  TH1F* off_iso_pt_hist; //!
  TH1F* off_05iso_pt_hist; //!
  TH1F* off_absiso_pt_hist; //!
  TH1F* deltar_hist; //!
  TH1F* transversemass_hist; //!
  TH1F* ftk50pt_off_pt_hist; //!
  TH1F* off50pt_ftk_pt_hist; //!

  TH1F* ftk_pt100_iso_hist; //!
  TH1F* ftk_pt75_iso_hist; //!
  TH1F* ftk_pt50_iso_hist; //!
  TH1F* ftk_full_eta_hist; //!
  TH1F* ftk_mu_deltar_hist; //!
  TH1F* delta_qpt_hist; //!

  TH1F* offline_hits_hist; //!  
  TH1F* isolation_hist; //! 
  TH2F* deltapt_eta_hist; //!
 
  int event_num; //!
  int ftk_num; //!
  int off_num; //!
  int off_match_num; //!
   
  int trig_hlt_iso50_num; //!
  int trig_hlt_iso50_mu20_num; //!
  int trig_hlt_iso50_mu26im_num; //!
  int trig_hlt_off_iso50_num; //!
  int trig_hlt_off_iso50_mu20_num; //!
  int trig_hlt_off_iso50_mu26im_num; //!
  int trig_l1_xe50_num; //!
  int trig_l1_mu20_num; //!
  int trig_hlt_mu26im_num; //!
  int trig_hlt_xe120_num; //!
  int trig_hlt_e50_num; //!
  int trig_hlt_off_05iso50_num; //!
  int trig_hlt_off_05iso75_num; //!
  int trig_hlt_off_iso50_e50_num; //!
  int trig_hlt_off_05iso75_e50_num; //!
  int trig_hlt_off_iso50_ftk_iso50_num; //!
  int trig_hlt_ftk_05iso75_num; //

  int trig_hlt_off_iso50_xe50_num; //!
  int trig_hlt_off_05iso50_xe50_num; //!
  int trig_hlt_off_iso75_xe50_num; //!
  int trig_hlt_off_05iso75_xe50_num; //!
  int trig_hlt_ftk_iso50_xe50_num; //!
  int trig_hlt_ftk_05iso50_xe50_num; //!
  int trig_hlt_ftk_iso75_xe50_num; //!
  int trig_hlt_ftk_05iso75_xe50_num; //!
  int trig_hlt_both_iso50_xe50_num; //!
  int trig_hlt_both_05iso50_xe50_num; //!
  int trig_hlt_both_iso75_xe50_num; //!
  int trig_hlt_both_05iso75_xe50_num; //!
  //int trig_hlt_iso50_xe50_num; //!
  int trig_hlt_xe120_xe50_num; //!
  int highpt_track_num; //!
  int highpt_muon_num; //!
  int off_highpt_track_num; //!
  int off_highpt_muon_num; //!

  int ftk_pt10_iso_num; //!
  int ftk_pt20_iso_num; //!
  int ftk_pt50_iso_num; //!
  int ftk_pt10_noiso_num; //!
  int ftk_pt20_noiso_num; //!
  int ftk_pt50_noiso_num; //!
  int off_pt10_iso_num; //!
  int off_pt20_iso_num; //!
  int off_pt50_iso_num; //!
  int off_pt10_noiso_num; //!
  int off_pt20_noiso_num; //!
  int off_pt50_noiso_num; //!
  int ftk_pt10_iso_den; //!
  int ftk_pt20_iso_den; //!
  int ftk_pt50_iso_den; //!
  int ftk_pt10_noiso_den; //!
  int ftk_pt20_noiso_den; //!
  int ftk_pt50_noiso_den; //!
  int off_pt10_iso_den; //!
  int off_pt20_iso_den; //!
  int off_pt50_iso_den; //!
  int off_pt10_noiso_den; //!
  int off_pt20_noiso_den; //!
  int off_pt50_noiso_den; //!


  bool run_muon = false;
  bool run_ftk = true;
  bool run_ftk_muonmatch = true;
  bool run_ftk_offmatch = true;
  bool run_off = true;
  bool run_off_muonmatch = true;
  bool run_effplots = true;
};
