#include "TriggerPlots.C"

#include <map>
#include <string>
#include <vector>

#include "TH1F.h"
#include "TChain.h"


// string::find_first_of
#include <iostream>       // std::cout
#include <string>         // std::string
#include <cstddef>        // std::size_t         

#include <bitset>
#include <TMath.h>



using namespace std;

class displacedTrack : public TriggerPlots {
 public:
  displacedTrack(std::string fileName);

  displacedTrack(TTree *tree);

  ~displacedTrack();

  /// The only function needing implementation                                                                                                
  virtual void     Loop();

  /// Write all histograms                                                                                                                    
  void writeAllHists();

  /// Draw final plots                                                                                                                        
  Int_t drawPlots(string outputFile="displacedTrack.root");

  bool passL1Bool; // dummy right now, introduced to be able to look at events passing and not passing L1 separately 
  int nCombs;

 protected:

  ///Init histograms                                                                                                                          
  void InitHists();

  map<string, TH1*> allHists;
  map<int, string> histfinder;

};
